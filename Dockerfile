FROM alpine:latest

MAINTAINER Leo Cheron <leo@cheron.works>

RUN apk --no-cache add bash git openssh rsync && \
    mkdir -p ~root/.ssh && chmod 700 ~root/.ssh/ && \
    echo -e "Port 22\n" >> /etc/ssh/sshd_config

EXPOSE 22
